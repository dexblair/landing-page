function App() {
  return (
    <div className="min-h-screen flex flex-col text-white">
      <main className="container mx-auto px-6 pt-16 flex-1 text-center">

        <h2 className="text-2xl md:text-4xl lg:text-6xl uppercase">
          Dexter Blair
        </h2>

        <h1 className="text-3xl md:text-6xl lg:text-8xl uppercase font-black mb-8">
          a portfolio
        </h1>

        <div className="text-lg md:text-2xl lg:text-3xl py-2 px-4 md:py-4 md:px-10 lg:py-6 lg:px-12 bg-white bg-opacity-10 w-fit mx-auto mb-8 rounded-full">
          <a href="https://gitlab.com/dexblair">3 projects</a>
        </div>

        <div className="container flex mx-auto flex-col">
          <ul className="list-none list-inside text-center space-y-5">
            <li><a href="https://gitlab.com/dexblair/landing-page">this landing page</a></li>
            <li><a href="https://gitlab.com/work_projects3/report-database">simple report database</a></li>
            <li><a href="https://gitlab.com/work_projects3/guitools">pysimplegui based toolkit for excel tasks</a></li>
          </ul>
        </div>

      </main>

      <footer className="container w-full mx-auto p-6 flex flex-col md:flex-row items-center justify-between">
        <p>Built with Vite by Dexter Blair</p>

        <div className="flex -mx-6">
          <a href="#" className="mx-3 hover:opacity-80 duration-150">About Us</a> |
          <a href="https://gitlab.com/dexblair" className="mx-3 hover:opacity-80 duration-150">GitLab</a> |
          <a href="#" className="mx-3 hover:opacity-80 duration-150">Contact</a>
        </div>
      </footer>
    </div>
  )
}

export default App
